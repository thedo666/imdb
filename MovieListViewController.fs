module MovieListView

open System
open System.Drawing
open System.Net
open System.Threading
open System.Xml.Linq

open MonoTouch.UIKit
open MonoTouch.Foundation

type IMDBItem = { Title : string; Image : string option; Url : string }

let url = "http://imdbapi.org/?title={0}&type=xml&plot=simple&episode=1&limit=100&yg=0&mt=M&lang=en-US&offset=&aka=simple&release=simple&business=0&tech=0"

let xname name = XName.Get(name)    
let toOption (x:XElement) = if x <> null then Some(x.Value) else None
let GetData (element:XElement) =  
    let title = element.Element(xname "title").Value
    let url = element.Element(xname "imdb_url").Value
    let image = element.Element(xname "poster")|>toOption
    { Title = title; Image = image; Url = url }
    
let find_movie (title:string) = async {
    let url = String.Format(url, title)
    use client = new WebClient()
    let! result = client.AsyncDownloadString(Uri(url))
    let results = XDocument.Parse result
    return results.Root.Elements(xname "item")|>Seq.map(GetData)|>Seq.toArray
}

let loadImage (image:string) = async {
    use client = new WebClient()
    let! data = client.DownloadDataTaskAsync(image)|>Async.AwaitTask
    let data = NSData.FromArray data
    return new UIImage(data)
}

let (==>) evt f = f evt
let start = ()
let finish = ignore

[<Register ("MovieListViewController")>]
type MovieListViewController () as this =
    inherit UINavigationController ()
    
    let context = SynchronizationContext.Current
    
    let search _ = 
        let onSearching = new Event<_>()
        let searchView = { new UIViewController() with
            member x.ViewDidLoad() = 
                let searchField = new UITextView(new RectangleF(0.0f,10.0f,UIScreen.MainScreen.Bounds.Width, 40.0f))
                searchField.Font <- UIFont.FromName("Arial", 30.0f)
                let searchButton = new UIButton(UIButtonType.RoundedRect, Frame=RectangleF(0.0f,50.0f,UIScreen.MainScreen.Bounds.Width, 40.0f))
                searchButton.SetTitle("Search", UIControlState.Normal)
                x.View.Add searchField 
                x.View.Add searchButton
                searchButton.TouchDown.Add(fun f -> onSearching.Trigger searchField.Text)
            member this.Title = "IMDB Search"
        }  
        this.PushViewController(searchView, true)
        onSearching.Publish
        
    let show_results (evt:IEvent<string>) = 
        let navigate = Event<string>()
        evt.Add(fun searchTerm -> 
            let tableController = new UITableViewController(Title="Results")
            let tableView = tableController.TableView
            this.PushViewController( tableController, true)
            async { 
                let! results = find_movie searchTerm 
                do! Async.SwitchToContext context
                
                let path = NSBundle.MainBundle.PathForResource("Placeholder.png", null)
                let placeholder = UIImage.FromFile(path)      
                tableView.Source <- 
                        { new UITableViewSource() with 
                             member __.RowsInSection(table,  section) = results.Length
                             member __.GetCell(table, index) = 
                                let cell = match table.DequeueReusableCell "MyIdentifier" with
                                           | null -> new UITableViewCell(UITableViewCellStyle.Default, "MyIdentifier")
                                           | cell -> cell
                                
                                let result = results.[index.Row]
                                cell.TextLabel.Text <-  result.Title
                                cell.ImageView.Image <- placeholder
                                match result.Image with
                                | Some (image) -> async { let! image = loadImage image
                                                          cell.ImageView.Image <- image
                                                        }|> Async.StartImmediate
                                | _ -> ()
                                cell
                             member __.RowSelected(table, index) = 
                                navigate.Trigger results.[index.Row].Url
                        }     
                tableView.ReloadData() //Required to actually show data
            }|>Async.StartImmediate)
        navigate.Publish
        
    let open_web_page (evt:IEvent<string>) = 
        evt.Add(fun url -> 
            let controller = new UIViewController()
            let webView = new UIWebView(controller.View.Frame)
            webView.LoadRequest (new NSUrlRequest(new NSUrl(url)))
            controller.View.Add(webView)
            this.PushViewController(controller, true))
        
    override this.ViewDidLoad () =
        base.ViewDidLoad ()
        start ==> search ==> show_results ==> open_web_page ==> finish
        
    override this.ShouldAutorotateToInterfaceOrientation (orientation) = true

